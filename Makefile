env = ./.env.example

ifneq ("$(wildcard ./.env)","")
    env = ./.env
endif

docker = docker-compose -p ${APP_NAME} -f ./docker/docker-compose.yml --env-file ${env}

include ${env}
export

# Запустить установку проекта
.PHONY: install
install:
	echo ${env}
	rm -f composer.lock || echo "already deleted"
	cp .env.example .env
	${docker} up -d
	${docker} exec -u www-data php-fpm sh -c "composer install"
	${docker} exec -u www-data php-fpm sh -c "php artisan key:generate"
	${docker} exec -u www-data php-fpm sh -c "php artisan storage:link"
	${docker} exec -u www-data php-fpm sh -c "php artisan migrate:fresh --seed"
# Comment this 3 next lines if you don't use Filament
	${docker} exec -u www-data php-fpm sh -c "php artisan make:filament-user" 
	${docker} exec -u www-data php-fpm sh -c "php artisan vendor:publish --tag=filament-config" 
	${docker} exec -u www-data php-fpm sh -c "php artisan vendor:publish --tag=filament-translations" 

# Запустить Docker демона
.PHONY: run
run:
	${docker} up -d

# Остановить работу Docker'а
.PHONY: stop
stop:
	${docker} stop

# Удалить все контейнеры
.PHONY: down
down:
	${docker} down

# Зайти в терминал php-fpm
.PHONY: php
php:
	${docker} exec -u www-data php-fpm bash

# Зайти в терминал postgres
.PHONY: mysql
mysql:
	${docker} exec mysql bash

# Зайти в терминал nginx
.PHONY: nginx
nginx:
	${docker} exec nginx sh

# Зайти в терминал phpmyadmin
.PHONY: phpmyadmin
phpmyadmin:
	${docker} exec phpmyadmin bash
